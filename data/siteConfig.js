module.exports = {
  siteTitle: 'Bonjour! Mon nom est Maxime, je suis développeur web freelance en full remote',
  siteDescription: `CV en ligne de Maxime Chofflet, développeur web.`,
  keyWords: ['gatsbyjs', 'react', 'curriculum', 'cv', 'développeur', 'internet', 'web', 'site'],
  authorName: 'Maxime Chofflet',
  twitterUsername: '_Max73',
  gitlabUsername: 'maxime.simplon',
  authorAvatar: '/images/profil.jpeg',
  authorDescription: `Je suis freelance full-stack dans le développement web. Actuellement disponible et à la recherche de missions.`,
  Tools:`phpMyadmin, Bootstrap, Git, Netlify, Firebase.
  Anglais niveau B1.`,

  skills: [{
      name: 'HTML',
      level: 70
    },
    {
      name: 'CSS',
      level: 60
    },
    {
      name: 'Javascript',
      level: 40
    },
    {
      name: 'PHP',
      level: 40
    },
    {
      name: 'Wordpress',
      level: 80
    },
    {
      name: 'Symfony',
      level: 40
    },
    {
      name: 'React',
      level: 30
    },
    {
      name: 'GatsbyJS',
      level: 30
    },
    {
      name: 'Angular',
      level: 20
    },
    {
      name: 'SQL',
      level: 30
    }
  ],
  jobs: [ {
      company: "Matching Emplois",
      begin: {
         month: 'sep',
        year: '2019'
      },
      duration: '2 mois, Savoie Technolac',
      occupation: "Développeur full-stack",
      description: `⟶ Développement sur Wordpress ⟶ Développement d'un bot de scraping ⟶  Développement d'un chatbot`
    },
    {
      company: "Simplon",
      begin: {
        month: 'jan',
        year: '2019'
      },
      duration: "9 mois, Chambéry",
      occupation: "Formation développeur web",
      description: "⟶ Réalisation d'un site entreprise responsive ⟶ Réalisation d'un site associatif complet ⟶ Réalisation d'un jeu en JavaScript ⟶ Réalisation d'un bot récupérateur de données en PHP ⟶ Insertion et mise en situation professionnelle ⟶ Répondre à la demande d'un client et cibler ses besoins ⟶ Accessibilité web ⟶ Travail en équipe"
    }, {
      company: "Ambulances HUNAULT",
      begin: {
        month: '2018 2017',
      },
      duration: '1 an, Metz',
      occupation: "Auxiliaire ambulancier",
    },
    {
      company: "AFTRAL",
      begin: {
        month: 'déc',
        year: '2016'
      },
      duration: '1 mois, Metz',
      occupation: "Diplôme auxiliaire ambulancier",
      description: ""
    },
    {
      company: "Vignoble International Prestige",
      begin: {
        month: 'fév',
        year: '2016',
      },
      duration: '3 mois, Montpellier',
      occupation: "Commercial",
    },
    {
      company: "CFPPA Agropolis",
      begin: {
        month: '2015 2013',
      },
      duration: '2ans, Montpellier',
      occupation: "BTSA Viticulture-Oenologie",

    }, {
      company: "Université Paul Verlaine",
      begin: {
        month: '2012 2011',
      },
      duration: '1 an, Metz',
      occupation: "L1 Information-Communication",
    }, {
      company: "UCPA",
      begin: {
        month: 'déc',
        year: '2011',
      },
      duration: 'France',
      occupation: "Diplôme du BAFA",
    }, {
      company: "Lycée Louis de Cormontaigne",
      begin: {
        month: '2011 2008',

      },
      duration: '3 ans, Metz',
      occupation: "Diplôme Baccalauréat économique et social",
    },
    /* ... */
  ],
  social: {
    twitter: "https://twitter.com/_Max73",
    linkedin: "https://www.linkedin.com/in/maxime-chofflet-a18635180/",
    gitlab: "https://gitlab.com/maxime.simplon",
    email: "maxime.chofflet@gmail.com"
  },
  siteUrl: 'https://santosfrancisco.github.io/gatsbystarter-cv',
  pathPrefix: '/gatsby-starter-cv', // Note: it must *not* have a trailing slash.
  siteCover: '/images/bg.jpg',
  googleAnalyticsId: 'UA-131359385-1',
  background_color: '#ffffff',
  theme_color: '#25303B',
  display: 'minimal-ui',
  icon: 'src/assets/d004bb7fcfd697c36923aac070e431c9.png',
  headerLinks: [{
    label: 'CV | 06 36 57 09 56 | maxime.chofflet@gmail.com',
    url: '/',
  }]
}